/**
 * example of validator object
 *
 * {
 *     field: name of param waited inside request
 *     predicate: function to test value upon
 *     predicateErrorMsg: message to send in case of value validation failed
 * }
 *
**/

/**
 *
 * @param arr
 * @returns {boolean}
 */
const empty = (arr) => arr.length === 0;

/**
 *
 * @param validators
 * @param parameters
 * @returns array
 */
const checkMissingParameters = (validators, parameters) => {
    return validators.reduce((errors, validator) => {
        const parameter = Object.keys(parameters).filter(parameter => parameter === validator.field)

        if (empty(parameter)) {
            let message = 'Missing parameter : ' + validator.field;
            errors.push(message);
            return errors;
        }

        return errors;
    }, []);
}

/**
 *
 * @param validators
 * @param parameters
 * @returns {*[]}
 */
const checkParameters = (validators, parameters) => {
    return Object.keys(parameters).reduce((errors, field) => {
        let validator = validators.filter(validator => validator.field === field)

        if (empty(validator)) {
            let message = 'Wrong parameter provided : ' + field;
            errors.push(message);
            return errors;
        }

        validator = validator[0];

        if (!validator.predicate(parameters[field])) {
            errors.push(validator.predicateErrorMsg);
            return errors;
        }

        return errors;
    }, []);
}

/**
 *
 * @param validators
 * @param parameters
 * @returns {Array}
 */
export const validate = (validators, parameters) => {
    let errors = checkMissingParameters(validators, parameters)

    if (!empty(errors))
        return errors;

    return checkParameters(validators, parameters);
}