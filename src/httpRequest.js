export const badRequest = (res, data) => {
    res.status(400);
    res.json({
        errors: data
    });
}

export const notFoundRequest = (res, data) => {
    res.status(404);
    res.json({
        data: []
    });
}

export const createdRequest = (res, data) => {
    res.status(201);
    res.json({
        data: data
    })
};

export const accessDeniedRequest = (res, data) => {
    res.status(403);
    res.json({
        data: data
    })
};