//EXAMPLE
import mysql from 'mysql';
import dotenv from 'dotenv';

dotenv.config();

const extranetHost = process.env.MYSQL_DB_EXTRANET_HOST;
const extranetUser = process.env.MYSQL_DB_EXTRANET_USER;
const extranetPassword = process.env.MYSQL_DB_EXTRANET_PASSWORD;
const extranetDatabase = process.env.MYSQL_DB_EXTRANET_DATABASE;

let dbExtranet = mysql.createConnection({
    host     : extranetHost,
    user     : extranetUser,
    password : extranetPassword,
    database : extranetDatabase
});


dbExtranet.connect();

export default {
    "dbExtranet": dbExtranet,
}


